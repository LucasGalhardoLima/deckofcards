# deckofcards

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
### Run tests using cypress
```
yarn run cypress open

open the home_page_spec.js file on the cypress panel that will open
```

### Run using docker (make sure you are logged in Docker)
```
sudo docker build -t deck-of-cards:dev .
sudo docker run -v ${PWD}:/app -v /app/node_modules -p 8081:8080 --rm deck-of-cards:dev
```

### Run using docker-compose (make any changes in the docker-compose file as you need)
```
sudo docker-compose up -d --build

```

### Usage
```
click the 'Draw a Card' button;

it should draw at least one card;

draw as many as 10 cards;

then select a Rotation Card;

click the 'Submit' button to create a pile and go to the pile page;

all the cards you drawed should be displayed ordered by the Rotation Card rank and suit;

the Rotation Card should be displayed below the cards, and after it should be the High Card;

if you drawed 5 cards or more, if your hand had at least one triple and one double, you should be able to see the Full House combinations of your hand;
```