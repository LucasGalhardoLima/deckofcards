describe('Deck of Cards - Intricately Test', function () {
    let rotation_card = ''
    let high_card = ''
    let hand = 0
    let first_cards_drawed = ["2H", "3H", "2D", "3D", "2C", "3C", "2S"]
    let card_order = [
        "2",
        "A",
        "K",
        "Q",
        "J",
        "0",
        "9",
        "8",
        "7",
        "6",
        "5",
        "4",
        "3"
    ]
    let suit_order = ["H", "D", "C", "S"]
    let handCardNumbers = []
    let handCardSuits = []
    let combinations = []

    it('successfully loads', function () {
        cy.visit('/') // change URL to match your dev URL
        cy.wait(2000)
        /*for (let i = 0; i <= 10; i++) {

            cy.get('.draw-btn').click()
            cy.wait(1000)
        }*/


    })

    it('draws cards from the deck', function () {
        //select how many cards will be drawed
        hand = 2

        //draw as many cards as the hand is able to
        for (let i = 0; i < hand; i++) {

            cy
                .get('.draw-btn').click()
                .wait(2000)

        }

    })

    it('verify if the hand is 10 cards or less', function () {
        //check for hand quantity
        cy
            .get('.deck-card')
            .should(($d) => {
                if ($d.length !== hand) {
                    throw new Error('Cards quantity different from Hand quantity')
                }

                if ($d.length > 10) {
                    throw new Error('The quantity of the cards should be no more than 10.')
                }

                $d.map((i, el) => first_cards_drawed.push(el.textContent.slice(0, 2)))
            })
    })

    it('verify if the Draw A Card button is disabled after drawing 10 cards', function () {
        //check if draw button is disabled
        cy
            .get('button')
            .should(($div) => {
                expect($div).to.have.class('v-btn--disabled')
            })
    })


    it('get rotation card', function () {
        //change AS (card code) to the desireble rotation card
        rotation_card = 'AS'
        cy
            .get('[data-cy=select-input]').type(`${rotation_card}{enter}`, {
                force: true
            })
    })

    if ('verify if the rotation card appeared have the same code as the rotation card selected', function () {
        //check for the rotation card code    
        cy
                .get('.rotation-card')
                .find('div')
                .find('p')
                .should(($div) => {
                    expect($div).to.have.length(2)

                    //const className = $div[0].className
                })
                .then(($div) => {
                    //change text to rotation card value without the code
                    expect($div[0]).to.have.text(rotation_card.slice(0,1))
                })
        })



        it('create a pile and go to piles page', function () {
            //create pile by submiting and going to next page, then check if the page url contains the correct route
            cy
                .get('.submit-btn').click()
                .wait(1000)
                .url().should('include', '/piles/')

        })

    it('verify if the text of the rotation card is the same as the rotation card previously selected', function () {
        //check if has the same rotation card as the one selected
        cy
            .get('p')
            .get('.rotation-card')
            .should('include.text', rotation_card)
    })



    it('check the high card', function () {
        //get the high card from the pile order
        cy
            .get('.pile-cards')
            .find('.v-card')
            .find('.pile-card-code')
            .should(($c) => {
                high_card = $c.get(0).innerText
            })
        //compare with the high card text
        cy
            .wait(1000)
            .get('p')
            .get('.high-card')
            .should(($h) => {
                expect($h.get(0).innerText).to.include(high_card)
            })

    })


    it('check full house combinations', function () {
        //do a simple check if it has a first 'valid' combination or if it is showing the correct text
        cy
            .wait(1000)
            .get('p')
            .get('.full-house')
            .find('span')
            .should(($s) => {
                if ($s.hasClass('no-combinations'))

                    if ($s.get(0).innerText !== 'None')
                        throw new Error('Has no combinations but its not showing the right text.')
                if ($s.hasClass('combinations')) {
                    combinations = $s[0].children[0].innerText.split('\n')

                    let cards = combinations[0].split(",").filter(x => x)

                    if(cards.length !== 5) {
                        throw new Error('First combination is invalid.')
                    } else if(combinations.length === 0) {
                        throw new Error('No combinations found.')
                    }
                }
            })

    })

})