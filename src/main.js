import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

import VuexStore from './store'
const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api = axios.create({
  baseURL: 'https://deckofcardsapi.com/api/deck/'
})


Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
