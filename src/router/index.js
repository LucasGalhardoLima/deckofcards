import Vue from 'vue'
import VueRouter from 'vue-router'
//import Home from '../views/Home.vue'
import Cards from '../views/Cards.vue'
import Piles from '../views/Piles.vue'
Vue.use(VueRouter)

const routes = [
  /* fixed route only to redirect to cards, will not be used in this project */
  {
    path: '/',
    redirect: '/cards',
    name: 'Home',
    component: Cards,

  }, 
  {
    path: '/cards',
    name: 'Cards',
    component: Cards
  }, 
  {
    path: '/piles/:deck_id/:pile_name',
    name: 'Piles',
    component: Piles
  }
]

const router = new VueRouter({
  routes
})

export default router