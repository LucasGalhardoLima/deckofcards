import system from './system'
import cards from './cards'
export default {
    modules: {
        system,
        cards
    }
}