export default {
    'set_new_deck'(state, value) {
        state.new_deck = value
    },
    'set_new_hand'(state, value) {
        //return console.log(value)
        state.new_hand.push(value)
    },
    'set_list_pile'(state, value) {
        state.new_pile = value
    },
    'set_rotation_card'(state) {
        state.rotation_card = JSON.parse(sessionStorage.getItem('rotation_card'))
    }
}