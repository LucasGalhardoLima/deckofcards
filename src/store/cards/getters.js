export default {
    getNewDeck: state => state.new_deck,
    getNewHand: state => state.new_hand,
    getNewPile: state => state.new_pile,
    getRotationCard: state => state.rotation_card,
    getHandNewOrder: state => {
        /* card values */
        let card_order = [
            "2",
            "A",
            "K",
            "Q",
            "J",
            "0",
            "9",
            "8",
            "7",
            "6",
            "5",
            "4",
            "3"
        ]
        /* card suits */
        let suit_order = ["H", "D", "C", "S"]
        /* new arrays for separation */
        let handCardNumbers = []
        let handCardSuits = []

        /* verifing state for the application not to break */
        if (state.rotation_card) {
            /* new array given the rotation card value and index in card_order array */
            let newOrder = card_order.splice(card_order.indexOf(state.rotation_card.rotation_card.slice(0, 1)));

            /* new array given the rotation card suit and index in suit_order array */
            let newSuitsOrder = suit_order.splice(
                suit_order.indexOf(state.rotation_card.rotation_card.slice(1))
            );

            /* creating new array with the desired order based on the rotation card value and suit */
            let reOrderCards = [
                ...newOrder,
                ...card_order
            ];
            let reOrderSuits = [
                ...newSuitsOrder,
                ...suit_order
            ];

            /* verifing if the state has the pile for the application not to break */
            if (Object.keys(state.new_pile)[0]) {
                /* extracting cards values and suits in separate arrays */
                handCardNumbers = state.new_pile[Object.keys(state.new_pile)[0]].cards.map(_ => _.code.slice(0, 1));

                handCardSuits = state.new_pile[Object.keys(state.new_pile)[0]].cards.map(_ => _.code.slice(1));
            }

            let newArray = []

            /* reordering cards values and suits based on the previously created arrays*/
            handCardNumbers.sort(function (a, b) {
                return reOrderCards.indexOf(a) - reOrderCards.indexOf(b);
            });

            handCardSuits.sort(function (a, b) {
                return reOrderSuits.indexOf(a) - reOrderSuits.indexOf(b);
            });

            //console.log('ordered-suits', reOrderSuits, 'ordered-cards', reOrderCards, 'hand-cards', handCardNumbers, 'hand-suits', handCardSuits)

            /*merging card value and suit in the new order array for the component to pick up*/
            handCardNumbers.map((c, posa) => {
                handCardSuits.map((s, posb) => {
                    state.new_pile[Object.keys(state.new_pile)[0]].cards.map((h,i) => {
                        if (c == h.code.slice(0,1) && s == h.code.slice(1))
                            newArray.push(c + s)
                    })
                })
            })
            //return console.log(newArray, reOrderCards, reOrderSuits);
            let unique = [...new Set(newArray)]
            
            return unique
        }
    },
}