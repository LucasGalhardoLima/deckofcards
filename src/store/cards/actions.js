export default {
    async setNewDeck({
        getters,
        commit
    }) {
        let {
            data
        } = await getters.getApi.get(`new/shuffle/?deck_count=1`)
        commit('set_new_deck', data)
    },

    async setNewDraw({
        getters,
        commit
    }, deck_id) {
        let {
            data
        } = await getters.getApi.get(`${deck_id}/draw/?count=1`)
        commit('set_new_hand', data.cards[0])
    },

    async setNewPile({
        getters,
        dispatch
    }, {
        deck_id,
        pile_name,
        cards
    }) {
        pile_name ? pile_name = pile_name : pile_name = `teste`
        let {
            data
        } = await getters.getApi.get(`${deck_id}/pile/${pile_name}/add/?cards=${cards}`)
        //console.log(data)
        dispatch('listPile', {
            deck_id,
            pile_name
        })
    },

    async listPile({
        getters,
        commit
    }, {
        deck_id,
        pile_name
    }) {
        let {
            data
        } = await getters.getApi.get(`${deck_id}/pile/${pile_name}/list/`)
        commit('set_list_pile', data.piles)
    },

    setRotationCard({
        commit
    }) {
        commit('set_rotation_card')
    }
}